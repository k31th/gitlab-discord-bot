const winston = require('winston');
winston.info('Starting bridge');

const express = require('express');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');
const config = require('../config');

exports.config = config;

const sslOptions = {
    key: fs.readFileSync(__dirname + '/../key.pem'),
    cert: fs.readFileSync(__dirname + '/../cert.pem'),
	passphrase: 'Highway420'
};

const router = require('./router');

var app = express();
app.use(bodyParser.json());
app.use('/api/v1', router);

https.createServer(sslOptions, app).listen(config.server.port, function() {
	winston.info('Listening on localhost:' + config.server.port);
});
