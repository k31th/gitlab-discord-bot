const util = require('util');

class Event {
    format(body) {
        return new Promise(function(resolve) {
            const buildStatus = body.build_status;

            if (buildStatus === 'running' ||
                buildStatus === 'failed' ||
                buildStatus === 'success') {

                var embed = {};
                const buildName = body.build_name;
                const buildDuration = body.build_duration;
                const buildStarted = new Date(body.build_started_at);
                const buildEnded = new Date(body.build_finished_at);

                const projectName = body.project_name;
                const currentBranch = body.ref;

                embed.url = body.repository.homepage + '/-/jobs/' + body.build_id;
                embed.color = 0xe80b0b;

                switch (buildStatus) {
                    case 'running':
                        embed.title = util.format('%s triggered a build for %s', buildName, projectName);
                        embed.description = util.format('Deployment started at **%s**', buildStarted.toLocaleTimeString());
                        break;
                    case 'success':
                    case 'failed':
                        const successFail = buildStatus === 'success' ? "SUCCESS" : "FAILURE"

                        embed.description = util.format('Deployment **%s**.\nBranch: %s\nDuration: %d seconds\nTime Completed: %s', successFail, currentBranch, buildDuration, buildEnded.toLocaleTimeString());
                        embed.title = util.format('Build status for %s', projectName);
                        break;
                }

                resolve({embeds: [embed]});
            } else {
                resolve({embeds: []});
            }
        });
    }
}

module.exports = Event;